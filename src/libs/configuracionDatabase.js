// exportamos un objeto

module.exports = {
    database: 'sisemaUsuarios',
    username: '',
    password: '',
    params: {
        dialect: 'sqlite',
        storage: 'sistemaUsuarios.sqlite',
        define: {
            underscore: true
        },
        operatorsAliases: false
    }
};