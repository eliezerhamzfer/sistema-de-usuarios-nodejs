import passport from 'passport';
import session from 'express-session';
import morgan from 'morgan';

module.exports = function (app){
    app.use(session({
        secret: '5emZKMYUB9C2vT6',
        resave: false,
        saveUninitialized: false
    }));
    app.use(morgan('dev'));
    app.use(passport.initialize());
    app.use(passport.session());    
    app.set('port', process.env.PORT || 3000);
}