import passport from 'passport';
import passportlocal from 'passport-local'

const LocalStrategy = passportlocal.Strategy;

//metodo de autenticacion, lo llamamos local-login
//este metodo recibe un objeto de configuracion, y un callback
module.export = function(app){
    const  Usuario = app.configDB.modelos.Usuario;
    passport.serializeUser((usuario, done) => {
        done(null, user.id);
    });
    passport.deserializeUser(async (id, done) => {
        let usuario = await Usuario.findById(id);
        done(null, usuario);
    });
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallBack: true
    }, async (req, email, password, done) => {
        let usuario = new Usuario();
        usuario.email = email;
        usuario.password = password; 
        await user.save();
        done(null, usuario);
        //User.create(Object.assign(req.body, { password: hash }))
    }))
};